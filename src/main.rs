extern crate rand;
use rand::thread_rng;
use rand::distributions::{Range,Sample};

use std::collections::HashMap;


fn main() {
    let diceware = load_diceware();
    let words = get_words(diceware,4);
    for n in words {
        println!("{:}", n);
    }
}

fn get_words(dw:HashMap<&str,&str>,mut n:u8) -> Vec<String> {
    let mut words = vec!();
    while n>0 {
        if let Some(word) = dw.get(roll_dice().as_str()) {
            words.push(word.to_string());
            n -= 1;
        }
    }

    words
}

fn roll_dice() -> String {
    let mut rng = thread_rng();
    let mut roll = String::new();
    for _ in 0..5 {
        roll.push_str(&Range::new(1, 7).
                      sample(&mut rng).to_string());
    }
    roll
}

fn load_diceware () -> HashMap<&'static str,&'static str> {
    let words = include_bytes!("diceware.asc");
    
    let mut diceware: HashMap<&str,&str> = HashMap::new();
    
    let s = match std::str::from_utf8(words) {
        Ok(v) => v,
        Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    };
    let words = s.split('\n').collect::<Vec<&str>>();
    
    for (i,word) in words.iter().enumerate() {
        if i < 1 { continue }
        let t = word.split('\t').collect::<Vec<&str>>();

        if t.len() > 1 {
            diceware.insert(t[0],t[1]);
        }
    }

    diceware
}
